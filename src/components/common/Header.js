import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Header = ({title, notifications, menu}) => {

    const { header, titleBox, titleBoxText, notifBox, notifBoxContent, menuBox } = styles;

    return (
        <View style={header}>
            <View style={titleBox}>
                <Text style={titleBoxText}>{title}</Text>
            </View>
            <View style={notifBox}>
                <Text style={notifBoxContent}>
                    {notifications}
                </Text>
            </View>
            <View style={menuBox}>{menu}</View>
        </View>
    )
};

const styles=StyleSheet.create({
    header: {
        height: 30,
        marginTop: 20,
        flexDirection: 'row',
        backgroundColor: '#7fffd4',
        shadowColor: '#000',
        shadowOffset: {width: 2, height: 2 },
        shadowOpacity: .5,
        marginBottom: 10
    },
    titleBox: {
        flex: 7,
        flexDirection: 'row'
    },
    titleBoxText: {
        color: 'darkslategrey',
        fontSize: 20,
        alignSelf: 'center',
        marginLeft: 10
    },
    notifBox: {
        flex: 1,
        backgroundColor: '#fff',
        flexDirection: 'row'
    },
    notifBoxContent: {
        alignSelf: 'center',
        marginHorizontal: 8
    },
    menuBox: {
        flex: 1
    }
})
export { Header };