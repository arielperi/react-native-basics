/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { AppRegistry, View } from 'react-native';
import App from './src/components/app`';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

export default class AwesomeProject extends Component {
  render() {
    return (
      <Provider store={createStore}>
        <View style={{flex: 1}}>
          <App />
        </View>
      </Provider>
    );
  }
}

AppRegistry.registerComponent('AwesomeProject', () => AwesomeProject);
