import React, { Component } from 'react';
import { 
    View, 
    Text,
    StyleSheet, 
    TouchableWithoutFeedback, 
    LayoutAnimation 
} from 'react-native';
import { connect } from 'react-redux';
import { CardSection } from './common';
import * as actions from '../actions';

class ListItem extends Component {


    componentWillUpdate() {
        LayoutAnimation.spring();
    }

    renderDescription(descStyle) {

        const { library, expanded } = this.props; 

        if(expanded) {
            return (
                <CardSection>
                    <Text style={descStyle}>
                        {library.description}
                    </Text>    
                </CardSection>    
            )
        }
    }

    render() {

        const { titleStyle, descStyle } = styles;

        const { id, title } = this.props.library;
        return (
            <TouchableWithoutFeedback onPress={() => this.props.selectLibrary(id)}>
                <View>
                    <CardSection>
                        <Text style={titleStyle}>{title}</Text>
                    </CardSection>
                    {this.renderDescription(descStyle)}
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

const styles = StyleSheet.create({
    titleStyle: {
        fontSize: 20,
        paddingLeft: 15
    },
    descStyle: {
        paddingLeft: 20,
        paddingRight: 20
    }
})

const mapStateToprops = (state, ownProps) => {
    const expanded = state.selectedLibraryId === ownProps.library.id;

    return { expanded };
}

export default connect(mapStateToprops, actions)(ListItem);