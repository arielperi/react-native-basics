import React from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';

const Button = ({ onPress, children }) => {
    console.log(children);
    return (
        <TouchableOpacity style={styles.button} onPress={onPress}>
            <Text style={styles.buttonText}>{ children }</Text>
        </TouchableOpacity>    
    );
};

const styles = StyleSheet.create({
    button: {
        flex: 1,
        alignSelf: 'stretch',
        backgroundColor: '#fff',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#007aff',
        marginLeft: 5,
        marginRight: 5,
        minHeight: 40
    },
    buttonText: {
        alignSelf: 'center',
        color: '#007aff',
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center'
    }
});

export { Button };
