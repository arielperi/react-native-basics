import React from 'react';
import { View } from 'react-native';
import { Header } from './common';
import LibraryList from './LibraryList';

const App = () => {
    return (
        <View>
            <Header title="super" notifications="lots"/>
            <LibraryList />
        </View>
    )
}

export default App;